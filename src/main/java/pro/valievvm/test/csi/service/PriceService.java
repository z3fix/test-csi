package pro.valievvm.test.csi.service;

import pro.valievvm.test.csi.model.Price;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

import static pro.valievvm.test.csi.util.PriceUtils.isOneTypePrice;

public class PriceService {

    /**
     *Метод объединения цен
     */
    public List<Price> mergePrices(List<Price> currentPrices, List<Price> newPrices) {

        if (newPrices == null || newPrices.isEmpty()) {
            return currentPrices;
        }

        for (Price newPrice : newPrices) {
            List<Price> resultPrices = new ArrayList<>();

            //Получаем цены пересекабщиеся с новой ценой
            List<Price> overlapOldPrice = currentPrices.stream().filter(oldPrice -> isPriceOverlap.test(oldPrice, newPrice)).collect(Collectors.toList());

            for (Price oldPrice : overlapOldPrice) {
                if (oldPrice.getBegin().equals(newPrice.getBegin()) && oldPrice.getEnd().equals(newPrice.getEnd())) {
                    //Если равны даты начала и конца действия, то удаляем для дальнейшего замещения новой ценой
                    currentPrices.remove(oldPrice);
                } else {
                    boolean rightOverlap = oldPrice.getBegin().before(newPrice.getBegin());
                    boolean leftOverlap = oldPrice.getEnd().after(newPrice.getEnd());
                    if (rightOverlap && leftOverlap) {
                        //Если период новой цены находится внутри старой, то клонируем старую цену
                        Price clonePrice = new Price(oldPrice);
                        clonePrice.setBegin(newPrice.getEnd());
                        resultPrices.add(clonePrice);

                        oldPrice.setEnd(newPrice.getBegin());
                    } else {
                        if (rightOverlap) {
                            oldPrice.setEnd(newPrice.getBegin());
                        } else {
                            oldPrice.setBegin(newPrice.getEnd());
                        }

                        //Удаляем старую цену если её перекрыли полностью
                        if (oldPrice.getBegin().after(oldPrice.getEnd())) {
                            currentPrices.remove(oldPrice);
                        }
                    }
                }
            }
            resultPrices.add(newPrice);

            //Добавляем новые цены в currentPrices для обработки их при последующих итерациях
            currentPrices.addAll(resultPrices);
        }

        return concatPrices(currentPrices);
    }

    /**
     * Сортирует и объеденяет одинаковые смежные цены в одну запись
     */
    public List<Price> concatPrices(List<Price> prices) {

        prices.sort(new PriceComparator());

        Price previousPrice = null;
        for (ListIterator<Price> i = prices.listIterator(); i.hasNext(); ) {

            Price price = i.next();

            if (previousPrice == null || !(isOneTypePrice(price, previousPrice)
                    && price.getBegin().equals(previousPrice.getEnd())
                    && price.getValue() == previousPrice.getValue())) {
                previousPrice = price;
            } else {
                previousPrice.setEnd(price.getEnd());
                i.remove();
            }

        }

        return prices;
    }

    /**
     * Проверяет пересекаются ли цены
     */
    private static final BiPredicate<Price, Price> isPriceOverlap = (oldPrice, newPrice) ->
            isOneTypePrice(oldPrice, newPrice)
                    && oldPrice.getBegin().before(newPrice.getEnd())
                    && newPrice.getBegin().before(oldPrice.getEnd());

    /**
     * Компаратор для сортировки цен по коду продукта, отделу, номеру и дате начала действия
     */
    private class PriceComparator implements Comparator<Price> {

        @Override
        public int compare(Price o1, Price o2) {

            int i = o1.getProductCode().compareTo(o2.getProductCode());
            if (i != 0) return i;

            i = Integer.compare(o1.getDepart(), o2.getDepart());
            if (i != 0) return i;

            i = Integer.compare(o1.getNumber(), o2.getNumber());
            if (i != 0) return i;

            return o1.getBegin().compareTo(o2.getBegin());
        }
    }
}
