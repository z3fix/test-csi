package pro.valievvm.test.csi.util;

import pro.valievvm.test.csi.model.Price;

public class PriceUtils {

    /**
     * Проверяет что бы цены были одного типа, т.е. имели один код продукта, департамент и номер
     */
    public static boolean isOneTypePrice(Price price1, Price price2) {

        return price1.getProductCode().equals(price2.getProductCode())
                && price1.getDepart() == price2.getDepart()
                && price1.getNumber() == price2.getNumber();
    }

}
