package pro.valievvm.test.csi;

import org.junit.Test;
import pro.valievvm.test.csi.service.PriceService;

import static org.junit.Assert.assertEquals;
import static pro.valievvm.test.csi.data.PriceStaticData.*;

public class PriceServiceTest {

    /**
     * Тест метода слияния одинаковых цен.
     */
    @Test
    public void testConcatPrice() {

        assertEquals(resultConcatPrice, new PriceService().concatPrices(noConcatPrice));
    }

    /**
     * Пример объединения из задания. В нем тестируется слияние двух одинаковых цен и разбитие начальной цены на 2 части.
     */
    @Test
    public void testMergePriceTZ() {

        assertEquals(resultPricesTZ, new PriceService().mergePrices(currentPricesTz, newPricesTZ));
    }

    /**
     * Тестируется удаление цены при полном замещении несколькими новыми ценами.
     */
    @Test
    public void testMergeAndRemove() {

        assertEquals(resultPricesRemove, new PriceService().mergePrices(currentPricesRemove, newPricesRemove));
    }

    /**
     * Тестируется полное замещение цены новой ценой.
     */
    @Test
    public void testMergeEqualsDate() {

        assertEquals(resultPricesEqualsDate, new PriceService().mergePrices(currentPricesEqualsDate, newPricesEqualsDate));
    }

    /**
     * Тестируется поведение при пересечении двух новых цен.
     */
    @Test
    public void testNewPriceOverlap() {

        assertEquals(resultPricesOverlap, new PriceService().mergePrices(currentPricesOverlap, newPricesOverlap));
    }


}
