package pro.valievvm.test.csi.data;

import pro.valievvm.test.csi.model.Price;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import static java.util.Arrays.asList;

public class PriceStaticData {

    public static ArrayList<Price> currentPricesTz, newPricesTZ, resultPricesTZ,
            currentPricesRemove, newPricesRemove, resultPricesRemove,
            currentPricesEqualsDate, newPricesEqualsDate, resultPricesEqualsDate,
            currentPricesOverlap, newPricesOverlap, resultPricesOverlap,
            noConcatPrice, resultConcatPrice;

    static DateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

    static {
        try {

            currentPricesTz = new ArrayList<>(asList(
                    new Price(1, "122856", 1, 1, format.parse("01.01.2013 00:00:00"),
                            format.parse("31.01.2013 23:59:59"), 11000),
                    new Price(2, "122856", 2, 1, format.parse("10.01.2013 00:00:00"),
                            format.parse("20.01.2013 23:59:59"), 99000),
                    new Price(3, "6654", 1, 2, format.parse("01.01.2013 00:00:00"),
                            format.parse("31.01.2013 00:00:00"), 5000)
            ));
            newPricesTZ = new ArrayList<>(asList(
                    new Price("122856", 1, 1, format.parse("20.01.2013 00:00:00")
                            , format.parse("20.02.2013 23:59:59"), 11000),
                    new Price("122856", 2, 1, format.parse("15.01.2013 00:00:00"),
                            format.parse("25.01.2013 23:59:59"), 92000),
                    new Price("6654", 1, 2, format.parse("12.01.2013 00:00:00"),
                            format.parse("13.01.2013 00:00:00"), 4000)
            ));
            resultPricesTZ = new ArrayList<>(asList(
                    new Price("122856", 1, 1, format.parse("01.01.2013 00:00:00"),
                            format.parse("20.02.2013 23:59:59"), 11000),
                    new Price("122856", 2, 1, format.parse("10.01.2013 00:00:00"),
                            format.parse("15.01.2013 00:00:00"), 99000),
                    new Price("122856", 2, 1, format.parse("15.01.2013 00:00:00"),
                            format.parse("25.01.2013 23:59:59"), 92000),
                    new Price("6654", 1, 2, format.parse("01.01.2013 00:00:00"),
                            format.parse("12.01.2013 00:00:00"), 5000),
                    new Price("6654", 1, 2, format.parse("12.01.2013 00:00:00"),
                            format.parse("13.01.2013 00:00:00"), 4000),
                    new Price("6654", 1, 2, format.parse("13.01.2013 00:00:00"),
                            format.parse("31.01.2013 00:00:00"), 5000)
            ));


            currentPricesRemove = new ArrayList<>(asList(
                    new Price("122856", 1, 1, format.parse("01.01.2013 00:00:00"),
                            format.parse("15.01.2013 00:00:00"), 80),
                    new Price("122856", 1, 1, format.parse("15.01.2013 00:00:00"),
                            format.parse("21.01.2013 00:00:00"), 87),
                    new Price("122856", 1, 1, format.parse("21.01.2013 00:00:00"),
                            format.parse("31.01.2013 23:59:59"), 90)
            ));
            newPricesRemove = new ArrayList<>(asList(
                    new Price("122856", 1, 1, format.parse("10.01.2013 00:00:00")
                            , format.parse("18.01.2013 00:00:00"), 80),
                    new Price("122856", 1, 1, format.parse("18.01.2013 00:00:00"),
                            format.parse("26.01.2013 00:00:00"), 85)
            ));
            resultPricesRemove = new ArrayList<>(asList(
                    new Price("122856", 1, 1, format.parse("01.01.2013 00:00:00"),
                            format.parse("18.01.2013 00:00:00"), 80),
                    new Price("122856", 1, 1, format.parse("18.01.2013 00:00:00"),
                            format.parse("26.01.2013 00:00:00"), 85),
                    new Price("122856", 1, 1, format.parse("26.01.2013 00:00:00"),
                            format.parse("31.01.2013 23:59:59"), 90)
            ));


            currentPricesEqualsDate = new ArrayList<>(asList(
                    new Price("122856", 1, 1, format.parse("01.01.2013 00:00:00"),
                            format.parse("11.01.2013 00:00:00"), 11000),
                    new Price("122856", 1, 1, format.parse("11.01.2013 00:00:00"),
                            format.parse("20.01.2013 00:00:00"), 99000),
                    new Price("122856", 1, 1, format.parse("20.01.2013 00:00:00"),
                            format.parse("31.01.2013 23:59:59"), 11000)
            ));
            newPricesEqualsDate = new ArrayList<>(asList(
                    new Price("122856", 1, 1, format.parse("11.01.2013 00:00:00"),
                            format.parse("20.01.2013 00:00:00"), 12000)
            ));
            resultPricesEqualsDate = new ArrayList<>(asList(
                    new Price("122856", 1, 1, format.parse("01.01.2013 00:00:00"),
                            format.parse("11.01.2013 00:00:00"), 11000),
                    new Price("122856", 1, 1, format.parse("11.01.2013 00:00:00"),
                            format.parse("20.01.2013 00:00:00"), 12000),
                    new Price("122856", 1, 1, format.parse("20.01.2013 00:00:00"),
                            format.parse("31.01.2013 23:59:59"), 11000)
            ));


            currentPricesOverlap = new ArrayList<>(asList(
                    new Price("122856", 1, 1, format.parse("01.01.2013 00:00:00"),
                            format.parse("11.01.2013 00:00:00"), 11000)
            ));
            newPricesOverlap = new ArrayList<>(asList(
                    new Price("122856", 1, 1, format.parse("09.01.2013 00:00:00"),
                            format.parse("20.01.2013 00:00:00"), 12000),
                    new Price("122856", 1, 1, format.parse("15.01.2013 00:00:00"),
                            format.parse("31.01.2013 00:00:00"), 13000)
            ));
            resultPricesOverlap = new ArrayList<>(asList(
                    new Price("122856", 1, 1, format.parse("01.01.2013 00:00:00"),
                            format.parse("09.01.2013 00:00:00"), 11000),
                    new Price("122856", 1, 1, format.parse("09.01.2013 00:00:00"),
                            format.parse("15.01.2013 00:00:00"), 12000),
                    new Price("122856", 1, 1, format.parse("15.01.2013 00:00:00"),
                            format.parse("31.01.2013 00:00:00"), 13000)
            ));


            noConcatPrice = new ArrayList<>(asList(
                    new Price("122853", 1, 1, format.parse("11.01.2013 00:00:00"),
                            format.parse("21.01.2013 00:00:00"), 11000),
                    new Price("122853", 1, 1, format.parse("01.01.2013 00:00:00"),
                            format.parse("11.01.2013 00:00:00"), 11000),
                    new Price("122853", 1, 1, format.parse("21.01.2013 00:00:00"),
                            format.parse("31.01.2013 23:59:59"), 11000)
            ));
            resultConcatPrice = new ArrayList<>(asList(
                    new Price("122853", 1, 1, format.parse("01.01.2013 00:00:00"),
                            format.parse("31.01.2013 23:59:59"), 11000)
            ));
        } catch (Exception ex) {
        }
    }
}
